<?php

/**
 * @file
 * IP address manager module install file.
 */

/**
 * Implements hook_schema().
 */
function ip_schema() {
  $schema['ip_tracker'] = [
    'description' => 'Stores IP addresses against uids.',
    'fields' => [
      'uid' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The User ID',
      ],
      'ip' => [
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'description' => 'The User IP address',
      ],
      'visits' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'How many visits with this uid/ip combination',
      ],
      'first_visit' => [
        'description' => 'A Unix timestamp indicating when this record was created.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'last_visit' => [
        'description' => 'A Unix timestamp indicating when this record was updated.',
        'type' => 'int',
        'not null' => TRUE,
      ],
    ],
    'primary key' => ['uid', 'ip'],
  ];

  $schema['ip_posts'] = [
    'description' => 'Stores ips against nids/cids.',
    'fields' => [
      'type' => [
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'default' => '',
        'description' => 'The entity type.',
      ],
      'id' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The entity id',
      ],
      'ip' => [
        'type' => 'varchar',
        'length' => 128,
        'not null' => TRUE,
        'description' => 'The User IP address',
      ],
    ],
    'primary key' => ['id', 'type'],
  ];

  return $schema;
}

/**
 * Allow saving IPv6 ips into database
 */
function ip_update_10001(&$sandbox) {
  $database = \Drupal::database();
  $schema = $database->schema();

  // Drop primary key first since ip is part of the key.
  $schema->dropPrimaryKey('ip_tracker');
  $schema->changeField('ip_tracker', 'ip', 'ip', ['type' => 'varchar', 'length' => 128]);
  $schema->addPrimaryKey('ip_tracker', ['uid', 'ip']);

  $schema->changeField('ip_posts', 'ip', 'ip', ['type' => 'varchar', 'length' => 128]);
}

/**
 * Convert IPv4 integers into A.B.C.D notation in ip_tracker table
 */
function ip_update_10002(&$sandbox) {
  $database = \Drupal::database();

  $results = $database->select('ip_tracker', 't')
    ->fields('t', ['uid', 'ip'])
    ->execute()->fetchAll();

  foreach ($results as $result) {
    $database->update('ip_tracker')
      ->fields([
        'ip' => long2ip($result->ip),
      ])
      ->condition('uid', $result->uid)
      ->condition('ip', $result->ip)
      ->execute();
  }
}

/**
 * Convert IPv4 integers into A.B.C.D notation in ip_posts table
 */
function ip_update_10003(&$sandbox) {
  $database = \Drupal::database();

  $results = $database->select('ip_posts', 't')
    ->fields('t', ['type', 'id', 'ip'])
    ->execute()->fetchAll();

  foreach ($results as $result) {
    $database->update('ip_posts')
      ->fields([
        'ip' => long2ip($result->ip),
      ])
      ->condition('id', $result->id)
      ->condition('type', $result->type)
      ->execute();
  }
}

