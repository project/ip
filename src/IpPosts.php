<?php

/**
 * @file
 * Contains Drupal\ip\IpPosts
 */

namespace Drupal\ip;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\HttpFoundation\Request;

/*
 * @file
 * IpTracker Manager.
 */

class IpPosts {

  private $connection;

  private $entity;

  private $request;

  function __construct(Connection $connection, Request $request, EntityInterface $entity) {
    $this->connection = $connection;
    $this->request = $request;
    $this->entity = $entity;
  }

  /**
   * Save the IpTrack
   */
  function save() {
    $ip = $this->request->getClientIp();

    if (!empty($ip) && is_numeric($this->entity->id())) {
      $this->connection->insert('ip_posts')
        ->fields(
          [
            'type' => $this->entity->getEntityTypeId(),
            'id' => $this->entity->id(),
            'ip' => $ip,
          ]
        )
        ->execute();
    }
  }

  /**
   * Remove records in the ip_posts table for a certain entity.
   */
  function remove() {
    $this->connection->delete('ip_posts')
      ->condition('type', $this->entity->getEntityTypeId())
      ->condition('id', $this->entity->id())
      ->execute();
  }

}
