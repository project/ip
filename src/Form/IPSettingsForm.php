<?php

namespace Drupal\ip\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * IP Settings Form
 */
class IPSettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private ModuleHandlerInterface $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ip_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['ip.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('ip.settings');
    $enabled = $this->moduleHandler->moduleExists('masquerade');

    $form['ignore_on_masquerade'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore IP Tracking while Masquerading'),
      '#default_value' => $config->get('ignore_on_masquerade'),
      '#description' => $this->t('Note: Can only be enabled if masquerade module is enabled.'),
      '#required' => FALSE,
      '#disabled' => !$enabled,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('ip.settings');

    $config
      ->set('ignore_on_masquerade', $form_state->getValue('ignore_on_masquerade'))
      ->save();
  }

}
