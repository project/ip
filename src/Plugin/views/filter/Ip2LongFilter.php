<?php

/**
 * @file
 * Contains \Drupal\ip\Plugin\views\filter\Ip2LongFilter.
 */

namespace Drupal\ip\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\NumericFilter;

/**
 * Filter to handle greater than/less than ip2long filters
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("ip2long")
 */
class Ip2LongFilter extends NumericFilter {

  function opBetween($field) {
    parent::opBetween($field);
  }

  function opSimple($field) {
    parent::opSimple($field);
  }

  function opEmpty($field) {
    parent::opEmpty($field);
  }

  function op_regex($field) {
    parent::opRegex($field);
  }

}
