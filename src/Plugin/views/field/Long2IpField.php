<?php

/**
 * @file
 * Contains \Drupal\ip\Plugin\views\field\Long2IpField.
 */

namespace Drupal\ip\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Render a long field as a ip value
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("long2ip")
 */
class Long2IpField extends FieldPluginBase {

  // @TODO option to link or not to address manage page
  function render(ResultRow $values) {
    return parent::render($values);
  }

}
